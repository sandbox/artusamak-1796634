<?php

/**
 * @file
 * Template that renders a part of the entity form.
 */

print drupal_render_children($form);
